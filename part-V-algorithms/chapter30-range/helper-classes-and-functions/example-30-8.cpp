/**
 * @brief Creating a range for an input stream with boost::istream_range()
 * */
#include <boost/range/algorithm.hpp>
#include <boost/range/istream_range.hpp>
#include <iostream>
#include <iterator>

using namespace std;

int main() {
	boost::iterator_range<istream_iterator<int>> ir = boost::istream_range<int>(std::cin);
	boost::copy(ir, ostream_iterator<int>{std::cout, "\n"});

	return 0;
}

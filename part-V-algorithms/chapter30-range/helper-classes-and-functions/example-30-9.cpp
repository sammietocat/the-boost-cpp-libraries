/**
 * @brief Creating ranges more easily with boost::sub_range()
 * */
#include <array>
#include <boost/range/algorithm.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/sub_range.hpp>
#include <iostream>
#include <iterator>

using namespace std;

int main() {
	array<int,6> a{{0,1,2,3,4,5}};

	boost::iterator_range<array<int,6>::iterator> r1 = boost::random_shuffle(a);
	//boost::sub_range<array<int,6>> r2 = boost::random_shuffle(r1);
	boost::sub_range<array<int,6>> r2 = boost::random_shuffle(a);

	boost::copy(r1, ostream_iterator<int>{std::cout, ","});
	cout << endl;
	boost::copy(r2, ostream_iterator<int>{std::cout, ","});
	cout << endl;

	return 0;
}

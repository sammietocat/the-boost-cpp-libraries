#include <boost/range/algorithm.hpp>
#include <boost/range/irange.hpp>
#include <iostream>

using namespace std;

int main() {
	boost::integer_range<int> ir = boost::irange(0,3);
	for(const auto& i:ir) {
		cout << i <<",";
	}
	cout << endl;

	return 0;
}

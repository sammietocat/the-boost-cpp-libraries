/**
 * @brief All algorithms from Boost.Range expect a range as a first parameter
 * */
#include <boost/range/algorithm.hpp>
#include <array>
#include <iostream>

using namespace std;

int main() {
	array<int, 6> a{{0,1,0,1,0,1}};
	cout << boost::count(a, 0) << endl;

	return 0;
}

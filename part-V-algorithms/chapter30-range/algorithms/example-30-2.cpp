/**
 * @brief Range algorithms related to algorithms from the standard library
 * */
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <array>
#include <iostream>
#include <iterator>

using namespace std;

int main() {
	array<int, 6> a{{0,1,2,3,4,5}};

	boost::random_shuffle(a);
	boost::copy(a, ostream_iterator<int>{std::cout, ","});

	cout << endl << *boost::max_element(a) << endl;
	cout << boost::accumulate(a, 0) << endl;

	return 0;
}

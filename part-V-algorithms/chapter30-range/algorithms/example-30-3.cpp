/**
 * @brief Range algorithms without counterparts in the standard library
 * */
#include <boost/range/algorithm_ext.hpp>
#include <array>
#include <deque>
#include <iostream>
#include <iterator>

using namespace std;

int main() {
	array<int, 6> a{{0,1,2,3,4,5}};
	cout << std::boolalpha << boost::is_sorted(a) << endl;

	deque<int> d;
	boost::push_back(d, a);
	boost::remove_erase(d, 2);
	boost::copy_n(d, 3, ostream_iterator<int>{std::cout, ","});
	cout << endl;

	return 0;
}

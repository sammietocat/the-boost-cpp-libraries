#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <array>
#include <iostream>
#include <iterator>

using namespace std;

int main() {
	array<int, 6> a{{0,5,2,1,3,4}};

	boost::copy(boost::adaptors::filter(a, [](int i){ return (i>2); }), ostream_iterator<int>{std::cout, ","});
	cout << endl;

	return 0;
}

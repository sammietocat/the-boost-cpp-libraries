#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/regex.hpp>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	string str = "The Boost C++ Libraries";
	boost::regex expr{"[\\w+]+"};

	boost::copy(boost::adaptors::tokenize(str, expr,0,boost::regex_constants::match_default), ostream_iterator<string>{std::cout, ","});

	return 0;
}

#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <array>
#include <iostream>
#include <iterator>
#include <map>

using namespace std;

int main() {
	array<int, 3> a{{0,1,2}};
	map<string,int*> m;

	m.insert(make_pair("a",&a[0]));
	m.insert(make_pair("b",&a[1]));
	m.insert(make_pair("c",&a[2]));


	boost::copy(boost::adaptors::keys(m), ostream_iterator<string>{std::cout, ","});
	cout << endl;
	boost::copy(boost::adaptors::indirect(boost::adaptors::values(m)), ostream_iterator<int>{std::cout, ","});
	cout << endl;

	return 0;
}

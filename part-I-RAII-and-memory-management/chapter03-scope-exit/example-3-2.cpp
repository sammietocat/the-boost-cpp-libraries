/**
 * @brief implement scope exit by lambda functions
 * */
#include <iostream>

using namespace std;

template<typename T>
struct scope_exit {
	scope_exit(T &&t_) : t(std::move(t_)) {}
	~scope_exit() {t();}

	T t;
};

template<typename T>
scope_exit<T> make_scope_exit(T &&t) {
	return scope_exit<T>(std::move(t));
}

int *foo() {
	int *i = new int(10);
	auto cleanup = make_scope_exit([&i]()mutable {delete i; i = nullptr; });
	cout << *i <<endl;
	return i;

}
int main() {
	int *j = foo();
	// should be a random address 
	cout << j << endl;

	return 0;
}

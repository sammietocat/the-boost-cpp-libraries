#include <boost/scope_exit.hpp>
#include <iostream>

using namespace std;

struct x {
	int i;

	void foo() {
		i = 10;

		// parameter must not be empty
		BOOST_SCOPE_EXIT(void) {
			cout << "last" <<endl;
		}BOOST_SCOPE_EXIT_END

		BOOST_SCOPE_EXIT(this_) {
			this_->i = 20;	
			cout << "first" <<endl;
		}BOOST_SCOPE_EXIT_END
	}
};

int main() {
	x obj;
	obj.foo();

	cout << obj.i <<endl;

	return 0;
}

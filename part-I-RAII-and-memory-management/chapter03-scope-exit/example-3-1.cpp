/**
 * @brief Using BOOST_SCOPE_EXIT
 * @details BOOST_SCOPE_EXIT is used to define a block that will be executed when the scope the block is defined in end
 * */
#include <boost/scope_exit.hpp>
#include <iostream>

using namespace std;

int *foo() {
	int *i = new int{10};

	// make `i` visible in the following scope
	// this block is executed before return and exceptions
	BOOST_SCOPE_EXIT(&i) {
		delete i;
		i = nullptr;
	} BOOST_SCOPE_EXIT_END

	cout << *i <<endl;
	return i;
}

int main() {
	int *j = foo();
	cout << j << endl;

	return 0;
}

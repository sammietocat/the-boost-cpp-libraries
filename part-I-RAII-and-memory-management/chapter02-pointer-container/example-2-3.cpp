#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ptr_container/ptr_inserter.hpp>
#include <array>
#include <algorithm>
#include <iostream>

using namespace std;

int main() {
	boost::ptr_vector<int> pvec;
	array<int,3> arr({0,1,2});
	copy(arr.begin(), arr.end(), boost::ptr_container::ptr_back_inserter(pvec));
	cout << pvec.size() <<endl;

	return 0;
}

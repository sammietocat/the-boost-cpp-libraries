/**
 * @brief boost::ptr_set with intuitively correct order
 * */
#include <boost/ptr_container/ptr_set.hpp>
#include <boost/ptr_container/indirect_fun.hpp>
#include <functional>
#include <iostream>
#include <memory>
#include <set>

using namespace std;

int main() {
	boost::ptr_set<int> s;
	s.insert(new int(2));
	s.insert(new int(1));
	cout << *s.begin() << endl;

	set<unique_ptr<int>,boost::indirect_fun<std::less<int>>> v;
	v.insert(unique_ptr<int>(new int(2)));
	v.insert(unique_ptr<int>(new int(1)));
	cout << **v.begin() << endl;

	return 0;
}

/**
 * @brief boost::ptr_vector, container for vector of pointers
 * */
#include <boost/ptr_container/ptr_vector.hpp>
#include <iostream>

int main() {
	boost::ptr_vector<int> pvec;
	pvec.push_back(new int{1});
	pvec.push_back(new int{2});

	std::cout << pvec.back() << std::endl;

	return 0;
}

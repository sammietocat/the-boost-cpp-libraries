#define BOOST_SP_USE_QUICK_ALLOCATOR
#include <boost/shared_ptr.hpp>
#include <iostream>
#include <chrono>

using namespace std;

int main() {
	auto start = std::chrono::system_clock::now();

	boost::shared_ptr<int> ptr;
	for(int i=0;i<1000000;++i){
		ptr.reset(new int{i});	
	}

	auto end = std::chrono::system_clock::now();
	cout << std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()<<" us"<<endl;

	return 0;
}

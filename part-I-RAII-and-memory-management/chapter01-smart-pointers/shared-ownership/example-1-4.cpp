/**
 * @brief	demo of boost::shared_ptr with a user-defined deleter
 * */
#include <boost/shared_ptr.hpp>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	boost::shared_ptr<int> ptr(new int{1}, [](const int* v){
				cout << "shared pointer with value "<<*v<<"  is to delete"<<endl;
				delete v;
			});
	cout << *ptr<<endl;

	return 0;
}

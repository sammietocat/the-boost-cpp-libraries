#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <functional>
#include <iostream>
#include <thread>

void reset(boost::shared_ptr<int> &sp) {
	std::cout << "reseting a shared pointer"<<std::endl;
	sp.reset();
}

void print(boost::weak_ptr<int> &wp) {
	std::cout << "print a weak pointer"<<std::endl;
	boost::shared_ptr<int> sp = wp.lock();	
	if(sp){
		std::cout << *sp<<std::endl;
	}
}

int main() {
	boost::shared_ptr<int> sp{new int{99}};
	boost::weak_ptr<int> wp{sp};

	std::thread t1{reset, std::ref(sp)};
	std::thread t2{print, std::ref(wp)};

	t1.join();
	t2.join();
	
	return 0;
}

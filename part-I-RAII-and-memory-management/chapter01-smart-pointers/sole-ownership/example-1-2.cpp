#include <boost/scoped_array.hpp>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	boost::scoped_array<int> p{new int[2]};

	*p.get() = 1;
	p[1] = 2;
	cout << p[0]<<","<<p[1]<<endl;

	p.reset(new int[3]);
	p[0] = 3;
	p[1] = 4;
	p[2] = 5;
	cout << p[0]<<","<<p[1]<<","<<p[2]<<endl;

	return 0;
}

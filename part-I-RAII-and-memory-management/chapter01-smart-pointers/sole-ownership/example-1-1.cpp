#include <boost/scoped_ptr.hpp>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	boost::scoped_ptr<int> ptr{new int{1}};
	cout << *ptr<<endl;

	ptr.reset(new int{2});
	cout << *ptr.get()<<endl;
	ptr.reset();
	cout << std::boolalpha<<static_cast<bool>(ptr)<<endl;

	return 0;
}

#include <boost/type_traits.hpp>
#include <iostream>

using namespace std;

int main() {
	cout.setf(ios::boolalpha);
	// boost::is_same expects two types as parameters and checks whether they are the same.
	cout << boost::is_same<boost::is_integral<int>::type, boost::true_type>::value << endl;
	cout << boost::is_same<boost::is_floating_point<int>::type, boost::false_type>::value << endl;
	cout << boost::is_same<boost::is_arithmetic<int>::type, boost::true_type>::value << endl;
	cout << boost::is_same<boost::is_reference<int>::type, boost::false_type>::value << endl;

	return 0;
}


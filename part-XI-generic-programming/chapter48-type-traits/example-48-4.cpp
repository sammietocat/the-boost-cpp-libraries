/**
 * @brief Changing type properties with Boost.TypeTraits
 * */
#include <boost/type_traits.hpp>
#include <iostream>

using namespace std;

int main() {
	cout.setf(ios::boolalpha);

	cout << boost::is_const<boost::add_const<int>::type>::value << endl;
	cout << boost::is_same<boost::remove_pointer<int*>::type,int>::value << endl;
	cout << boost::is_same<boost::make_unsigned<int>::type, unsigned int>::value << endl;
	cout << boost::is_same<boost::add_rvalue_reference<int>::type, int&&>::value << endl;

	return 0;
}


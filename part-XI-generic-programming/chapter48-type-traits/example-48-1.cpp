#include <boost/type_traits.hpp>
#include <iostream>

using namespace std;

int main() {
	cout.setf(ios::boolalpha);
	cout << boost::is_integral<int>::value << endl;
	cout << boost::is_floating_point<int>::value << endl;
	cout << boost::is_arithmetic<int>::value << endl;
	cout << boost::is_reference<int>::value << endl;

	return 0;
}


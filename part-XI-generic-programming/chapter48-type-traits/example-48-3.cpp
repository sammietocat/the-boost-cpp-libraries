/**
 * @brief Checking type properties with Boost.TypeTraits
 * */
#include <boost/type_traits.hpp>
#include <iostream>

using namespace std;

int main() {
	cout.setf(ios::boolalpha);

	// checks whether a type supports the operator `operator+`
	// and whether two objects of the same type can be concatenated.
	cout << boost::has_plus<int>::value<<endl;
	// checks whether a type supports the pre-increment operator `operator++`
	cout << boost::has_pre_increment<int>::value << endl;
	// checks whether a type has a trivial copy constructor
	cout << boost::has_trivial_copy<int>::value << endl;
	// checks whether a type has a virtual destructor
	cout << boost::has_virtual_destructor<int>::value << endl;

	return 0;
}


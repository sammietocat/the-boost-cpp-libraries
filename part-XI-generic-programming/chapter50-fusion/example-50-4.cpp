/**
 * @brief Accessing elements in Fusion containers with iterators
 * */
#include <boost/fusion/tuple.hpp>
#include <boost/fusion/iterator.hpp>
#include <boost/mpl/int.hpp>
#include <type_traits>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

struct print {
	template<typename T>
		void operator()(const T &t) const {
			cout << std::boolalpha << t << endl;
		}
};

int main() {
	typedef bf::tuple<int,string,bool,double> tuple_type;
	tuple_type t{10,"Boost",true,3.14};

	auto itr = begin(t);
	cout << *itr << endl;
	auto itr2 = bf::advance<boost::mpl::int_<2>>(itr);
	cout << std::boolalpha << *itr2 << endl;

	return 0;
}

/**
 * @brief Fusion support for std::pair
 * */
#include <boost/fusion/adapted.hpp>
#include <boost/fusion/sequence.hpp>
#include <boost/mpl/int.hpp>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

struct strct {
	int i;
	double d;
};

BOOST_FUSION_ADAPT_STRUCT(strct,
		(int, i)
		(double, d)
		)

int main() {
	auto p = std::make_pair(10,3.14);
	cout << bf::at<boost::mpl::int_<0>>(p) << endl;
	cout << bf::back(p) << endl;

	return 0;
}

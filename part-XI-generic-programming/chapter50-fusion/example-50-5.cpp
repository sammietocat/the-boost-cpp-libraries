#include <boost/fusion/container.hpp>
#include <boost/fusion/sequence.hpp>
#include <boost/mpl/int.hpp>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

int main() {
	typedef bf::vector<int, string, bool, double> vec_type;
	vec_type vec{10,"Boost",true, 3.14};
	cout << bf::at<boost::mpl::int_<0>>(vec) << endl; 

	auto vec2 = bf::push_back(vec,'X');
	cout << bf::size(vec) << endl;
	cout << bf::size(vec2) << endl;
	cout << bf::back(vec2) << endl;

	return 0;
}

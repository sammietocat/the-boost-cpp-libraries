/**
 * @brief Filtering a Fusion container with boost::fusion::filter_view
 * */
#include <boost/fusion/tuple.hpp>
#include <boost/fusion/view.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/mpl/arg.hpp>
#include <type_traits>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

struct print {
	template<typename T>
		void operator()(const T &t) const {
			cout << std::boolalpha << t << endl;
		}
};

int main() {
	typedef bf::tuple<int,string,bool,double> tuple_type;
	tuple_type t{10,"Boost",true,3.14};

	bf::filter_view<tuple_type,std::is_integral<boost::mpl::arg<1>>> v{t};
	bf::for_each(v, print{});

	return 0;
}

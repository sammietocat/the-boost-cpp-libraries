/**
 * @brief Iterating over a tuple with boost::fusion::for_each()
 * */
#include <boost/fusion/tuple.hpp>
#include <boost/fusion/algorithm.hpp>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

struct print {
	template<typename T>
		void operator()(const T &t) const {
			cout << std::boolalpha << t << endl;
		}
};

int main() {
	typedef bf::tuple<int,string,bool,double> tuple_type;
	tuple_type t{10,"Boost",true,3.14};

	bf::for_each(t, print{});

	return 0;
}

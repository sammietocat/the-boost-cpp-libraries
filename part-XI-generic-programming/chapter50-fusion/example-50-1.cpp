/**
 * @brief Processing Fusion tuples
 * */
#include <boost/fusion/tuple.hpp>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

int main() {
	typedef bf::tuple<int,string,bool,double> tuple_type;
	tuple_type t{10,"Boost",true,3.14};

	cout << bf::get<0>(t) << endl;
	cout << bf::get<1>(t) << endl;
	cout << std::boolalpha << bf::get<2>(t) << endl;
	cout << bf::get<3>(t) << endl;

	return 0;
}

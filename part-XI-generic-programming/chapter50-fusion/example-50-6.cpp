/**
 * @brief	A heterogeneous map with boost::fusion::map
 * */
#include <boost/fusion/container.hpp>
#include <boost/fusion/sequence.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/mpl/int.hpp>
#include <iostream>

namespace bf = boost::fusion;

using namespace std;

int main() {
	// keys are types
	auto m = bf::make_map<int,string,bool,double>("Boost",10,3.14,true);
	if(bf::has_key<string>(m)) {
		cout << bf::at_key<string>(m) << endl;
	}

	auto m2 = bf::erase_key<string>(m);
	auto m3 = bf::push_back(m2, bf::make_pair<float>('X'));
	cout << std::boolalpha << bf::has_key<string>(m3) << endl;

	return 0;
}

/**
 * @brief Specializing functions for a group of types with boost::enable_if
 * */
#include <type_traits>
#include <iostream>

using namespace std;

template<typename T>
void print(typename std::enable_if<std::is_integral<T>::value,T>::type i) {
	cout << "Integral: "<< i << endl;
}

template<typename T>
void print(typename std::enable_if<std::is_floating_point<T>::value,T>::type f) {
	cout << "Floating point: "<< f << endl;
}

int main() {
	print<short>(1);
	print<long>(2);
	print<double>(3.14);

	return 0;
}


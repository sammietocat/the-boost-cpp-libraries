/**
 * @brief Overloading functions with boost::enable_if on their return value
 * @details Boost.EnableIf makes it possible to disable overloaded function templates or specialized class templates. Disabling means that the compiler ignores the respective templates. This helps to prevent ambiguous scenarios in which the compiler doesn’t know which overloaded function template to use. It also makes it easier to define templates that can be used not just for a certain type but for a group of types. 
 * @note Since C++11, Boost.EnableIf has been part of the standard library. So all demos in this chapter will used the STL.
 * */

//#include <boost/utility/enable_if.hpp>
#include <type_traits>
#include <iostream>

using namespace std;

template<typename T>
typename std::enable_if<std::is_same<T,int>::value,T>::type create() {
	return 1;
}

template<typename T>
typename std::enable_if<std::is_same<T,string>::value,T>::type create() {
	return "Boost";
}

int main() {
	cout << create<string>() << endl;

	return 0;
}


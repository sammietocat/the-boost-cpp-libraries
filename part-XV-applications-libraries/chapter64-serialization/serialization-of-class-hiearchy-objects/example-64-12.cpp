/**
 * @brief Registering derived classes statically with BOOST_CLASS_EXPORT
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

class Animal {
	public:
		Animal() = default;
		Animal(int nLeg_) : nLeg(nLeg_) {}
		virtual int getLegs() const { return nLeg; }
		virtual ~Animal() = default;
	private:
		friend class boost::serialization::access;
		
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { ar & nLeg; }

		int nLeg;
};

class Bird : public Animal {
	public:
		Bird() = default;
		Bird(int nLeg_, bool canFly_): Animal(nLeg_), canFly(canFly_) {}
		bool isCanFly() const { return canFly; }
	private:
		friend class boost::serialization::access;
		
		/**
		 * inherited member variables are serialized by accessing the base class inside the member function serialize() of the derived class and calling boost::serialization::base_object().
		 * */
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { 
			ar & boost::serialization::base_object<Animal>(*this);
			ar & canFly;
		}


		bool canFly;
};

// The macro BOOST_CLASS_EXPORT must be used if objects of derived classes are to be serialized using a pointer to their corresponding base class. 
BOOST_CLASS_EXPORT(Bird)

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	Animal *a = new Bird(2,false);
	oa << a;
	delete a;
}

void load(){
	ba::text_iarchive ia{ss};
	Animal *a;

	ia >> a;
	cout << a->getLegs() <<endl;

	delete a;
}

int main() {
	save();
	load();

	return 0;
}

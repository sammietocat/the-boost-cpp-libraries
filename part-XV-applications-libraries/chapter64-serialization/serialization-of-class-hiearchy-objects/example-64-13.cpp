/**
 * @brief Registering derived classes dynamically with register_type()
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

class Animal {
	public:
		Animal() = default;
		Animal(int nLeg_) : nLeg(nLeg_) {}
		virtual int getLegs() const { return nLeg; }
		virtual ~Animal() = default;
	private:
		friend class boost::serialization::access;
		
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { ar & nLeg; }

		int nLeg;
};

class Bird : public Animal {
	public:
		Bird() = default;
		Bird(int nLeg_, bool canFly_): Animal(nLeg_), canFly(canFly_) {}
		bool isCanFly() const { return canFly; }
	private:
		friend class boost::serialization::access;
		
		/**
		 * inherited member variables are serialized by accessing the base class inside the member function serialize() of the derived class and calling boost::serialization::base_object().
		 * */
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { 
			ar & boost::serialization::base_object<Animal>(*this);
			ar & canFly;
		}


		bool canFly;
};

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	oa.register_type<Bird>();

	Animal *a = new Bird(2,false);
	oa << a;

	delete a;
}

void load(){
	ba::text_iarchive ia{ss};
	ia.register_type<Bird>();
	Animal *a;

	ia >> a;
	cout << a->getLegs() <<endl;

	delete a;
}

int main() {
	save();
	load();

	return 0;
}

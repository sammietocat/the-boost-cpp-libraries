/**
 * @brief Serializing derived classes correctly
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

class Animal {
	public:
		Animal() = default;
		Animal(int nLeg_) : nLeg(nLeg_) {}
		int getLegs() const { return nLeg; }
	private:
		friend class boost::serialization::access;
		
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { ar & nLeg; }

		int nLeg;
};

class Bird : public Animal {
	public:
		Bird() = default;
		Bird(int nLeg_, bool canFly_): Animal(nLeg_), canFly(canFly_) {}
		bool isCanFly() const { return canFly; }
	private:
		friend class boost::serialization::access;
		
		/**
		 * inherited member variables are serialized by accessing the base class inside the member function serialize() of the derived class and calling boost::serialization::base_object().
		 * */
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { 
			ar & boost::serialization::base_object<Animal>(*this);
			ar & canFly;
		}


		bool canFly;
};

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	Bird penguin(2,false);
	oa << penguin;
}

void load(){
	ba::text_iarchive ia{ss};
	Bird penguin;
	ia>>penguin;

	cout << std::dec<< penguin.getLegs() <<endl;
	cout << std::boolalpha << penguin.isCanFly() <<endl;
}

int main() {
	save();
	load();

	return 0;
}

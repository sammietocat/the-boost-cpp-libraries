/**
 * @brief Serializing references
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

class Animal {
	public:
		Animal() = default;
		Animal(int nLeg_) : nLeg(nLeg_) {}
		int getLegs() const { return nLeg; }
	private:
		friend class boost::serialization::access;
		
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { ar & nLeg; }

		int nLeg;
};

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	Animal a(4);
	Animal &aref = a;

	oa << aref;
}

void load(){
	ba::text_iarchive ia{ss};
	Animal a;
	Animal &aref = a;
	ia >> aref;

	cout << std::dec<< a.getLegs() <<endl;
}

int main() {
	save();
	load();

	return 0;
}

/**
 * @brief Serializing with a free-standing function
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

struct Animal {
	Animal() = default;
	Animal(int l) : nLegs(l) {}
	int legs() const { return nLegs; }

	int nLegs;
};

/**
 * @details 
 *   * 2nd parameter should be a reference to an object to be serialized
 *   * requires that essential member variables of a class can be accessed from outside
 * */
template<typename Archive>
void serialize(Archive &ar, Animal &a, const unsigned int version) {
	ar & a.nLegs;	
}

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	Animal animal{4};
	oa << animal;
}

void load(){
	ba::text_iarchive ia{ss};
	Animal animal;
	ia >> animal;
	cout << animal.legs() <<endl;
}

int main() {
	save();
	load();

	return 0;
}

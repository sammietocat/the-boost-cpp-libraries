/**
 * @brief  Using boost::archive::text_oarchive
 * @details The main concept of Boost.Serialization is the archive. An archive is a sequence of bytes that represent serialized C++ objects. Objects can be added to an archive to serialize them and then later loaded from the archive.
 * */
#include <boost/archive/text_oarchive.hpp>
#include <iostream>

namespace ba=boost::archive;

int main() {
	ba::text_oarchive toa{std::cout};
	int i = 1;
	toa << i;

	return 0;
}

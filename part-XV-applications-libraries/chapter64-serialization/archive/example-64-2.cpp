/**
 * @brief  Using boost::archive::text_iarchive
 * @details Constructors of archives expect an input or output stream as a parameter. The stream is used to serialize or restore data. 
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <fstream>
#include <iostream>

namespace ba=boost::archive;

using namespace std;

void save() {
	ofstream fout{"archive.txt"};
	ba::text_oarchive oa{fout};
	int i = 1;
	oa << i;
}

void load(){
	ifstream fin{"archive.txt"};
	ba::text_iarchive ia{fin};
	int i = 0;
	ia >> i;
	cout << i <<endl;
}

int main() {
	save();
	load();

	return 0;
}

/**
 * @brief Serializing with a stringstream
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	int i = 1;
	oa << i;
}

void load(){
	ba::text_iarchive ia{ss};
	int i = 0;
	ia >> i;
	cout << i <<endl;
}

int main() {
	save();
	load();

	return 0;
}

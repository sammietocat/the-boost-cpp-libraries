/**
 * @brief Serializing user-defined types with a member function
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

class Animal {
	public:
		Animal() = default;
		Animal(int l) : nLegs(l) {}
		int legs() const { return nLegs; }
	private:
		// make is possible for Boost.Serialization to access `serialize()`
		friend class boost::serialization::access;
		
		// MUST NAME THE FUNCTION AS `serialize()`
		// '&' is for both serialization and restoration
		// '>>' for restoration
		// '<<' for serialization
		// should be private
		template<typename Archive>
		void serialize(Archive &ar, const unsigned int version) { ar & nLegs; }

		int nLegs;
};

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	Animal animal{4};
	oa << animal;
}

void load(){
	ba::text_iarchive ia{ss};
	Animal animal;
	ia >> animal;
	cout << animal.legs() <<endl;
}

int main() {
	save();
	load();

	return 0;
}

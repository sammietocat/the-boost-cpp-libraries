/**
 * @brief Backward compatibility with version numbers
 * */
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>	// for serializing std::string
#include <iostream>
#include <sstream>

namespace ba=boost::archive;

using namespace std;

class Animal {
	public:
		Animal() = default;
		Animal(int nLeg_, string name_):nLeg(nLeg_), name(std::move(name_)) {}
		int getLegs() const { return nLeg; }
		const string &getName() const {return name;}

	private:
		friend class boost::serialization::access;

		template<typename Archive>
		friend void serialize(Archive &ar, Animal &a, const unsigned int version);

		int nLeg;
		string name;
};

template<typename Archive>
void serialize(Archive &ar, Animal &a, const unsigned int version) {
	ar & a.nLeg;
	if (version>0) {
		ar & a.name;
	}
}

// assign a version number to class Animal. Default is 0 if not specified
BOOST_CLASS_VERSION(Animal, 1)

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	Animal a(4,"cat");
	oa << a;
}

void load(){
	ba::text_iarchive ia{ss};
	Animal a;
	ia >> a;

	cout << a.getLegs() <<endl;
	cout << a.getName() <<endl;
}

int main() {
	save();
	load();

	return 0;
}

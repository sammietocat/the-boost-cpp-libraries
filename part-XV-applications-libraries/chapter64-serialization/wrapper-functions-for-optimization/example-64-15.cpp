/**
 * @brief Serializing an array with the wrapper function make_array() to save space
 * */
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/array.hpp>
#include <array>
#include <iostream>
#include <sstream>

namespace ba = boost::archive;
using namespace std;

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	std::array<int,3> arr = {0,1,2};
	oa << boost::serialization::make_array(arr.data(), arr.size());
}

void load() {
	ba::text_iarchive ia{ss};

	std::array<int, 3> arr;
	ia >> boost::serialization::make_array(arr.data(), arr.size());

	cout << arr[0] << ", "<< arr[1] << ", "<< arr[2] << endl;
}

int main() {
	save();
	load();

	return 0;
}

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/array.hpp>
#include <array>
#include <iostream>
#include <sstream>

namespace ba = boost::archive;
using namespace std;

stringstream ss;

void save() {
	ba::text_oarchive oa{ss};
	std::array<int,3> a = {0,1,2};
	oa << a;
}

void load() {
	ba::text_iarchive ia{ss};

	std::array<int, 3> arr;
	ia >> arr;

	cout << arr[0] << ", "<< arr[1] << ", "<< arr[2] << endl;
}

int main() {
	save();
	load();

	return 0;
}

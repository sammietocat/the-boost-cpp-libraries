
cmake_minimum_required(VERSION 3.8)
project(program-options CXX)

set(CMAKE_CXX_FLAGS "-std=c++11")
find_package(Boost REQUIRED COMPONENTS program_options)
#target_link_libraries (getting-started boost_program_options)

add_executable(example-63-1 example-63-1.cpp)
target_link_libraries(example-63-1 ${Boost_LIBRARIES})

add_executable(example-63-2 example-63-2.cpp)
target_link_libraries(example-63-2 ${Boost_LIBRARIES})

add_executable(example-63-3 example-63-3.cpp)
target_link_libraries(example-63-3 ${Boost_LIBRARIES})

add_executable(example-63-4 example-63-4.cpp)
target_link_libraries(example-63-4 ${Boost_LIBRARIES})

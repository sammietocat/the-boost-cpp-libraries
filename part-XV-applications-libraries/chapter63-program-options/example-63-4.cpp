/**
 * @brief	Positional options with Boost.ProgramOptions
 * */
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;
namespace po=boost::program_options;

int main(int argc, const char *argv[]){
	try {
		po::options_description generalOpts{"General"};
		generalOpts.add_options()
			("help,h", "Help screen")
			("config", po::value<string>(), "Config file");

		po::options_description fileOpts{"File"};
		fileOpts.add_options()
			("age", po::value<int>(), "Age");

		po::variables_map vm;
		po::store(po::parse_command_line(argc,argv,generalOpts), vm);

		if(vm.count("config")){
			ifstream fin{vm["config"].as<string>().c_str()};
			if(fin){
				po::store(po::parse_config_file(fin,fileOpts), vm);
			}
		}

		po::notify(vm);
		if(vm.count("help")) {
			cout << generalOpts<<endl;
			return 0;
		}
		if (vm.count("age")){
			cout << "Your age is: "<<vm["age"].as<int>()<<endl;
		}

	}catch(const po::error &err ){
		cerr << err.what()<<endl;
	}

	return 0;
}

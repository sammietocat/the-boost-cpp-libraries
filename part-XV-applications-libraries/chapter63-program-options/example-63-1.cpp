/**
 * @brief Basic approach with Boost.ProgramOptions
 * @details 3 critical steps
 *   * define command-line options
 *   * use a parser to evaluate the command line
 *   * store the command-line options evaluated by the parser into a variables map
 * */
#include <boost/program_options.hpp>
#include <iostream>

using namespace std;
namespace po=boost::program_options;

void onAge(int age){
	cout << "On age: "<<age<<endl;
}

int main(int argc, const char *argv[]) {
	try {
		po::options_description desc{"Options"};
		desc.add_options()
			("help,h", "Help screen")	// `h` is the short name, and should be placed after the command
			// pass a pointer to an object of type boost::program_options::value_semantic as the second parameter to operator() to define an option as a name/value pair.
			// default_value() is to provide a default value
			("pi", po::value<float>()->default_value(3.14),"Pi")
			("age", po::value<int>()->notifier(onAge),"Age");

		po::variables_map vm;
		po::store(parse_command_line(argc, argv, desc), vm);
		// trigger callbacks specified by `notifier()`
		po::notify(vm);

		if(vm.count("help")) {
			cout << desc <<endl;
		}
		if(vm.count("age")){
			// ensure the typename `int` as that of `add_options()`
			cout << "Age: "<<vm["age"].as<int>()<<endl;
		}
		if(vm.count("pi")){
			cout << "Pi: "<<vm["pi"].as<float>()<<endl;
		}
	}catch(const po::error &err){
		cerr << err.what()<<endl;	
	}

	return 0;
}

/**
 * @brief	Positional options with Boost.ProgramOptions
 * */
#include <boost/program_options.hpp>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

using namespace std;
namespace po=boost::program_options;

void toStdOut(const vector<string> &vec){
	std::copy(vec.begin(), vec.end(), std::ostream_iterator<string>{cout, "\n"});
}

int main(int argc, const char *argv[]){
	try {
		int age;

		po::options_description desc{"Options"};
		desc.add_options()
			("help,h", "Help screen")
			// `multitoken()` allows multiple tokens for `--phone`,
			// `zero_tokens()` make it possible for `--phone` to use with no phone number
			// `composing()` makes it possible to use `--phone` multiple times
			("phone", po::value<vector<string> >()->multitoken()->zero_tokens()->composing(), "Phone");

		po::positional_options_description posDesc;
		// must ensure `--phone` has been defined in `desc`
		posDesc.add("phone", -1);	// assign all positional options to `--phone`

		po::command_line_parser parser{argc, argv};
		// call `options()` to pass the definition of command-line options to the parser
		// `allow_unregistered()`: no exception throwing in case of unregistered options
		// `po::command_line_style::allow_slash_for_short)`: allows short name used with a slash, e.g. `-h` as `/h`
		parser.options(desc).positional(posDesc).allow_unregistered().style(
				po::command_line_style::default_style
				| po::command_line_style::allow_slash_for_short);
		// do the parsing job
		po::parsed_options parsedOpts = parser.run();

		po::variables_map vm;
		po::store(parsedOpts, vm);
		// make the storage of value for `--age` in `age` in effect
		po::notify(vm);

		if(vm.count("help")) {
			cout << desc<<endl;
			return 0;
		}
		if (vm.count("phone")){
			toStdOut(vm["phone"].as<vector<string> >());
		}

	}catch(const po::error &err ){
		cerr << err.what()<<endl;
	}

	return 0;
}

/**
 * @brief Generating random UUIDs with boost::uuids::random_generator 
 * @note UUIDs are universally unique identifiers that don’t depend on a central coordinating instance.
 * */
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>	// for generating UUID
#include <boost/uuid/uuid_io.hpp>
#include <iostream>

using namespace std;
namespace bu=boost::uuids;

int main(){
	// generator to make a UUID
	bu::random_generator randGen; 
	// id cannot be make itself
	bu::uuid id = randGen();

	cout << bu::to_string(id) <<endl;

	cout << boost::lexical_cast<string>(id)<<endl;

	return 0;
}

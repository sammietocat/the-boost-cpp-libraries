/**
 * @brief Generators from Boost.Uuid
 * */
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>	// for generating UUID
#include <boost/uuid/uuid_io.hpp>
#include <iostream>

using namespace std;
namespace bu=boost::uuids;

int main(){
	// generator to make a UUID
	bu::nil_generator nilGen;
	// id cannot be make itself
	bu::uuid id = nilGen();

	// nil means all 0s
	cout << std::boolalpha<<id.is_nil()<<endl;

	bu::string_generator strGen;
	id = strGen("CF77C981-F61B-7817-10FF-D916FCC3EAA4");
	// kind of id
	cout << id.variant()<<endl;

	bu::name_generator nameGen(id);
	cout << nameGen("theboostcpplibraries.com") <<endl;

	return 0;
}

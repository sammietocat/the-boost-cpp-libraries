/**
 * @brief Generating random UUIDs with boost::uuids::random_generator 
 * @note UUIDs are universally unique identifiers that don’t depend on a central coordinating instance.
 * */
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>	// for generating UUID
#include <boost/uuid/uuid_io.hpp>	// for printing UUID
#include <iostream>

using namespace std;
namespace buu=boost::uuids;

int main(){
	// generator to make a UUID
	buu::random_generator randGen; 
	// id cannot be make itself
	buu::uuid id = randGen();

	cout << id<<endl;

	return 0;
}

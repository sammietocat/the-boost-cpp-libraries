# Change Log for Reviewing `The Boost C++ Libraries`  

## [2017-08-22]  
+ **Added**  
	- Examples for [Chapter 64. Serialization](https://theboostcpplibraries.com/boost.serialization)  
	- Examples for [Chapter 03. Scope Exit](https://theboostcpplibraries.com/boost.scopeexit)  
	- Examples for [Chapter 02. Pointer Container](https://theboostcpplibraries.com/boost.pointer_container)    
	- Examples for [Chapter 07. Format](https://theboostcpplibraries.com/boost.format)   
	- Examples for [Chapter 06. Lexical Cast](https://theboostcpplibraries.com/boost.lexical_cast)  
	- Examples for [Chapter 35. File System](https://theboostcpplibraries.com/boost.filesystem)  
	- Examples for [Chapter 30. Range](https://theboostcpplibraries.com/boost.range)   
	- Examples for [Chapter 48. Type Traits](https://theboostcpplibraries.com/boost.typetraits)  
	- Examples for [Chapter 49. Enable If](https://theboostcpplibraries.com/boost.enableif)  
	- Examples for [Chapter 50. Fusion](https://theboostcpplibraries.com/boost.fusion)  

## [2017-08-21]  
+ **Added**  
	- `README.md` and `CHANGELOG.md`  
	- Examples for [Chapter 01. Smart Pointers](https://theboostcpplibraries.com/boost.smartpointers)  
	- Examples for [Chapter 63. Program Options](https://theboostcpplibraries.com/boost.program_options)  
	- Examples for [Chapter 65. Uuid](https://theboostcpplibraries.com/boost.uuid)  

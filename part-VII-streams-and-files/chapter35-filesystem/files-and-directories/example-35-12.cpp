#include <boost/filesystem.hpp>
#include <iostream>
#include <ctime>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"example-35-11"};

	try {
		time_t lwt = bfs::last_write_time(p);
		cout << ctime(&lwt) << endl;
	}catch(const bfs::filesystem_error &err) {
		cerr << err.what() << endl;
	}

	return 0;
}

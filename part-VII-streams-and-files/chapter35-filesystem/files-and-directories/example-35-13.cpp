#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"/"};

	try{
		bfs::space_info si = bfs::space(p);

		cout << si.capacity << endl;
		cout << si.free << endl;
		cout << si.available << endl;
	}catch(const bfs::filesystem_error &err) {
		cerr << err.what() << endl;
	}

	return 0;
}

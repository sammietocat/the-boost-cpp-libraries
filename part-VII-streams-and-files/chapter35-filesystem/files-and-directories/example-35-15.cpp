#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	try {
		cout << bfs::absolute("example-35-15") << endl;	
	}catch(const bfs::filesystem_error &err) {
		cerr << err.what() << endl;
	}
	return 0;
}

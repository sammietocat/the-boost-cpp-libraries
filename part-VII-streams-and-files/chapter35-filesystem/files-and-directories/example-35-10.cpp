/**
 * @brief Using boost::filesystem::status()
 * @details 2 variants of file handling functions
 *   * `boost::filesystem::filesystem_error`: exception style
 *   * `boost::system::error_code`: error code style
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"/home/loccs/Workspaces"};

	try {
		bfs::file_status s = status(p);
		cout << std::boolalpha <<"bfs::is_directory: "<< bfs::is_directory(s) << endl;
		cout << std::boolalpha <<"bfs::is_regular_file: "<< bfs::is_regular_file(s) << endl;
		cout << std::boolalpha <<"bfs::is_symlink: "<< bfs::is_symlink(s) << endl;
		cout << std::boolalpha <<"bfs::exists: "<< bfs::exists(s) << endl;
	}catch(const bfs::filesystem_error &err) {
		cerr << err.what() << endl;
	}

	return 0;
}

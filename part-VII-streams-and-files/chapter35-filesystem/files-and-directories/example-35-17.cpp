/**
 * @brief get current working directory
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	try {
		cout << bfs::current_path() << endl;
		bfs::current_path("/home/loccs/Workspaces");
		cout << bfs::current_path() << endl;
	}catch(const bfs::filesystem_error &err) {
		cerr << err.what() << endl;
	}

	return 0;
}

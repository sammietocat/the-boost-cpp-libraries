#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"example-35-11"};
	boost::system::error_code ec;
	boost::uintmax_t fileSize = bfs::file_size(p, ec);
	if(!ec) {
		cout << fileSize << endl;
	}else {
		cerr << ec << endl;
	}

	return 0;
}

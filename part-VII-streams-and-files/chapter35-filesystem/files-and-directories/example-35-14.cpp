#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"hello"};

	try {
		if(bfs::create_directory(p)) {
			bfs::rename(p, "hello2");
			bfs::remove("hello2");
		}	
	}catch(const bfs::filesystem_error &err) {
		cerr << err.what() << endl;
	}

	return 0;
}

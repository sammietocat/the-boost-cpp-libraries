/**
 * @brief demo of meaningless boost::filesystem::path
 * */
#include <boost/filesystem.hpp>
#include <iostream>

using namespace std;
namespace bfs = boost::filesystem;

int main() {
	// no checking for validity or existence
	bfs::path p1{"..."};
	bfs::path p2{"\\"};
	bfs::path p3{"@:"};

	cout << p1 << endl;
	cout << p2 << endl;
	cout << p3 << endl;

	return 0;
}

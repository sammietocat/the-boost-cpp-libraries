/**
 * @brief Initializing boost::filesystem::path with a portable path
 * @note Windows accepts the slash as a directory separator even though it prefers the backslash
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"/"};

	cout << p.string() << endl;
	cout << p.generic_string() << endl;

	return 0;
}

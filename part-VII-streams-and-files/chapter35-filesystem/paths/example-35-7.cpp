/**
 * @brief Iterating over components of a path
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"/home/loccs/Workspaces"};

	for(const bfs::path &pp: p) {
		cout << pp << endl;
	}

	return 0;
}

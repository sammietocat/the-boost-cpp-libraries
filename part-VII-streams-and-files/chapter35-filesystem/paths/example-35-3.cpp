/**
 * @brief Retrieving paths from boost::filesystem::path as strings
 * */
#include <boost/filesystem.hpp>
#include <iostream>

using namespace std;
namespace bfs = boost::filesystem;

int main() {
	bfs::path p{"C:\\Windows\\System"};

#ifdef BOOST_WINDOWS_API
	wcout << p.native() << endl;
#else
	cout << p.native() << endl;
#endif

	cout << p.string() << endl;
	wcout << p.wstring() << endl;

	// generic_string() and generic_wstring() both return paths in a generic format. 
	// Generic paths are therefore identical to paths used on Linux.
	cout << p.generic_string() << endl;
	wcout << p.generic_wstring() << endl;

	return 0;
}

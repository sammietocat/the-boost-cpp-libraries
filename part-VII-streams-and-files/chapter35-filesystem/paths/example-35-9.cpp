/**
 * @brief Concatenating paths with operator/=
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path windowsPath{"C:/Windows/System"};
	cout << windowsPath.make_preferred() << endl;

	bfs::path p{"/home"};
	p /= "loccs/Workspaces";

	cout << absolute(p) << endl;
	cout << p.string() << endl;

	return 0;
}

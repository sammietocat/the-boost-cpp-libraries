/**
 * @brief Receiving file name and file extension
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"CMakeCache.txt"};

	cout << p.stem() << endl;
	cout << p.extension() << endl;

	return 0;
}

/**
 * @brief Concatenating paths with operator/=
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"/home"};
	p /= "loccs/Workspaces";

	cout << p.string() << endl;

	return 0;
}

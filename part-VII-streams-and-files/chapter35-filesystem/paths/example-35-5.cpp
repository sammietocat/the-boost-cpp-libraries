/**
 * @brief Accessing components of a path
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"/home/sammy/Workspaces"};

	cout << p.root_name() << endl;
	cout << p.root_directory() << endl;
	cout << p.root_path() << endl;
	cout << p.relative_path() << endl;
	cout << p.parent_path() << endl;
	cout << p.filename() << endl;

	return 0;
}

#include <boost/filesystem/fstream.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p{"hello.txt"};
	bfs::ofstream fout{p};
	fout << "Hello World" << endl;

	return 0;
}

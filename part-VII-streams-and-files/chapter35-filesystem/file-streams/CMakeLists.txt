
cmake_minimum_required(VERSION 3.8)
project(boost-paths CXX)

set(CMAKE_CXX_FLAGS "-std=c++11")

find_package(Boost REQUIRED COMPONENTS filesystem)

add_executable(example-35-19 example-35-19.cpp)
target_link_libraries(example-35-19 ${Boost_LIBRARIES})

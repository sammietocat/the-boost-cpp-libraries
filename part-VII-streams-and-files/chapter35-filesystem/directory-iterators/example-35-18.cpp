/**
 * @brief Iterating over files in a directory
 * */
#include <boost/filesystem.hpp>
#include <iostream>

namespace bfs = boost::filesystem;

using namespace std;

int main() {
	bfs::path p = bfs::current_path();

	bfs::directory_iterator dItr{p};
	while(dItr!=bfs::directory_iterator{}) {
		cout << *dItr<< endl;
		++dItr;
	}
	cout << "***" <<endl;
	bfs::recursive_directory_iterator rcsDirItr{p};
	while(rcsDirItr!=bfs::recursive_directory_iterator{}) {
		cout << *rcsDirItr<< endl;
		++rcsDirItr;
	}

	return 0;
}

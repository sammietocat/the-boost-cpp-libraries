/**
 * @brief	boost::lexical_cast is an alternative to functions like std::stoi(), std::stod(), and std::to_string()
 * @details boost::lexical_cast uses streams internally to perform the conversion. Therefore, only types with overloaded operator<< and operator>> can be converted
 * */
#include <boost/lexical_cast.hpp>
#include <iostream>

using namespace std;

int main() {
	string s = boost::lexical_cast<string>(123);
	cout << s << endl;

	double d = boost::lexical_cast<double>(s);
	cout << d << endl;

	return 0;
}

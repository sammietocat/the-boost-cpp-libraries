/**
 * @brief boost::bad_lexical_cast in case of an error
 * */
#include <boost/lexical_cast.hpp>
#include <iostream>

using namespace std;

int main() {
	try {
		int i = boost::lexical_cast<int>("abc");
		cout << i << endl;
	} catch(const boost::bad_lexical_cast &err){
		cerr << err.what() << endl;
	}

	return 0;
}

/**
 * @brief  boost::format with seemingly invalid placeholders
 * */
#include <boost/format.hpp>
#include <iostream>

int main() {
	// format apply appropriate manipulators to configure the internal stream
	std::cout << boost::format{"%+s %s %s"} % 1 % 2 % 1 << std::endl;

	return 0;
}

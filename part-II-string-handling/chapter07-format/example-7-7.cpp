/**
 * @brief  boost::format with the syntax used from std::printf()
 * */
#include <boost/format.hpp>
#include <iostream>

int main() {
	/**
	 * The letter "d" within the format string does not indicate the output of a number. 
	 * Instead, it applies the manipulator std::dec() to the internal stream object used 
	 * by boost::format. This makes it possible to specify format strings that would make 
	 * no sense for std::printf() and would result in a crash.
	 * */
	std::cout << boost::format{"%+d %d %d"} % 1 % 2 % 1 << std::endl;

	return 0;
}

/**
 * @brief  boost::io::format_error in case of an error
 * */
#include <boost/format.hpp>
#include <iostream>

int main() {
	try {
		std::cout << boost::format{"%|+| %2% %1%"} % 1 % 2 << std::endl;
	}catch(const boost::io::format_error &err) {
		std::cout << err.what() << std::endl;	
	}

	return 0;
}

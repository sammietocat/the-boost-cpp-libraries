/**
 * @brief	boost::format is a type-safe and extensible alternative to std::printf()
 * */
#include <boost/format.hpp>
#include <iostream>

int main() {
	// format data with manipulators
	// std::showpos will append + sign to every non-negatives
	// due to `%|1$+|`, only the 1st occurrence will be manipulated
	std::cout << boost::format{"%|1$+| %2% %1%"} % 1 % 2 << std::endl;

	return 0;
}

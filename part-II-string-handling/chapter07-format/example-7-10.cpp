/**
 * @brief confusing!!!
 * */
#include <boost/format.hpp>
#include <iostream>

using namespace std;

struct animal {
	string name;
	int nLeg;

	operator const char*() const {
		return (name+","+to_string(nLeg)).c_str();
	}
};

/*
ostream &operator<<(ostream &os, const animal &a) {
	return os << a.name <<", "<<a.nLeg;
}
*/

int main() {
	animal a{"cat", 4};
	//cout <<"a.name = "<< a.name << endl;
	//cout <<"a.nLeg = "<< a.nLeg << endl;
	//cout << "hello = " << a <<endl;
	cout << boost::format{"%1%"} % a <<endl;

	return 0;
}

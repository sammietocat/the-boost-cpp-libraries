/**
 * @brief	boost::format is a type-safe and extensible alternative to std::printf()
 * */
#include <boost/format.hpp>
#include <iostream>

int main() {
	// format data with manipulators
	// std::showpos will append + sign to every non-negatives
	std::cout << boost::format{"%1% %2% %1%"} % boost::io::group(std::showpos, 1) % 2 << std::endl;

	return 0;
}

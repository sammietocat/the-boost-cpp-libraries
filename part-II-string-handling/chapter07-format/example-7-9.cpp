/**
 * @brief boost::format with user-defined type
 * */
#include <boost/format.hpp>
#include <iostream>

using namespace std;

struct animal {
	string name;
	int nLeg;
};

ostream &operator<<(ostream &os, const animal &a) {
	return os << a.name <<", "<<a.nLeg;
}

int main() {
	animal a{"cat", 4};
	cout << boost::format{"%1%"} % a <<endl;

	return 0;
}
